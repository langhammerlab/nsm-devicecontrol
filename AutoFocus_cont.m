function autoFocusFrames = AutoFocus_cont(camera,autoFocus,autoFocusFrames)
    camera.horisontalBinning = 1;
    autoFocus.camera = camera;
    camera.triggerMode = 'Software';
    camera.startAcquisition();
    
    [frames, time] = autoFocus.focusInZ(0.1);
    for i=1:size(frames,3)
        autoFocusFrames(:,:,end+1) = frames(:,:,i);

    end
    [frames, time] = autoFocus.focusInX();
    for i=1:size(frames,3)
        autoFocusFrames(:,:,end+1) = frames(:,:,i);

    end
    [frames, time] = autoFocus.focusInX();
    for i=1:size(frames,3)
        autoFocusFrames(:,:,end+1) = frames(:,:,i);

    end
    
    camera.stopAcquisition();
    imagesc(autoFocusFrames(:,:,end));
    pause(2);