# NSM-DeviceControl v1.0

## Description
Device Control code for 1D-biomolecule tracking as described in the paper "Label-Free Nanofluidic Scattering Microscopy of Size and Mass of Single Diffusing Molecules and Nanoparticles" [Link to paper TBA]. 
The software requires Matlab and can be used to collect images from an attached camera and save image data to files. 
The main code is designed to work with an Andor Zyla camera, and will have to be atapted to other models. 
The code also performed automatic focusing and positioning of samples using a MAd City Labs micro- and nano positioners. Drivers for these units has to be provided by the user. 



## Installation

- install matlab with the packages: 
- install matlab SDK3 from Andor
- install SDK for mad city labs positioners
- setup the main file (NSM_DeviceControl.m) to point to appropriate driver locations. 

## Usage
The software requires that a line shaped object is placed at the center of the field of view on the camera. Autofocusing will ensure that the object is centered perfectly and focus to achieve the maximum intensity in the center of the choosen ROI. 
camera settings and sample specifications has to be modified in the NSM_DeviceControl.m file. 

## Files 
- **_NSM_DeviceControl.m_** - Main matlab file that runs a measurement and saves files. Acquisition settings are edited in this files.
- **_ZylaCamera.m_** Matlab class that is used for communicating with a Andor Zyla camera through the Andor Matlab SDK3. 
- **_AutoFocus.m_** Matlab class responsible for controlling auto focus operations. designed to work with a camera class and micro controller classes.
