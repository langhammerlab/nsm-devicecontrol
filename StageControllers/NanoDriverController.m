classdef NanoDriverController<handle
    %NANODRIVERCONTROLLER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        handle
        currentXPositionInMicroMeter
        currentYPositionInMicroMeter
        currentZPositionInMicroMeter
    end
    
    methods
        function obj = NanoDriverController(libraryPath)
            obj.loadNanoDriveLibrary(libraryPath);
            obj.initialiseNanoDriveHandle();
        end
        
        function close(obj)
            calllib('Madlib', 'MCL_ReleaseHandle', obj.handle);
            unloadlibrary('Madlib');
            disp('Exiting');
        end
        
        function position = get.currentXPositionInMicroMeter(obj)
            position = calllib('Madlib','MCL_SingleReadN',1, obj.handle);
        end
        
        function position = get.currentYPositionInMicroMeter(obj)
            position = calllib('Madlib','MCL_SingleReadN',2, obj.handle);
        end
        
        function position = get.currentZPositionInMicroMeter(obj)
            position = calllib('Madlib','MCL_SingleReadZ',obj.handle);
        end
        
        function moveDistanceInXDirection(obj, distance)
            err = calllib('Madlib','MCL_SingleWriteN',obj.currentXPositionInMicroMeter + distance,1, obj.handle);
            obj.printErrorMessage(err);
         end
        
         function moveDistanceInYDirection(obj, distance)
            err = calllib('Madlib','MCL_SingleWriteN',obj.currentYPositionInMicroMeter + distance,2, obj.handle);
            obj.printErrorMessage(err);
         end
        
         function moveDistanceInZDirection(obj, distance)
            err = calllib('Madlib','MCL_SingleWriteZ',obj.currentZPositionInMicroMeter + distance, obj.handle);
            obj.printErrorMessage(err);
        end
        
         function moveToXPositionInMicroMeter(obj, positionInMicrometer)
            err = calllib('Madlib','MCL_SingleWriteN',positionInMicrometer,1, obj.handle);
            obj.printErrorMessage(err);
         end
        
         function moveToYPositionInMicroMeter(obj, positionInMicrometer)
            err = calllib('Madlib','MCL_SingleWriteN',positionInMicrometer,2, obj.handle);
            obj.printErrorMessage(err);
         end
        
         function moveToZPositionInMicroMeter(obj, positionInMicrometer)
            err = calllib('Madlib','MCL_SingleWriteZ',positionInMicrometer, obj.handle);
            obj.printErrorMessage(err);
        end
        
    end
    
    methods(Access=private)
        
        function loadNanoDriveLibrary(obj,libraryPath)
            disp(pwd);
            loadlibrary('Madlib', libraryPath); 
            if (~libisloaded('Madlib'))
                disp('Error: Madlib library did not load correctly');
                obj.close()
            end
        end
        
        function initialiseNanoDriveHandle(obj)
            obj.handle = calllib('Madlib', 'MCL_InitHandle');
            if (obj.handle == 0)
                disp('Error: Handle was not initialized correctly');
                obj.close()
            end
        end
        
        function printErrorMessage(obj,errorCode)
            switch errorCode
                case 0
                case -1
                    disp('MCL_GENERAL_ERROR');
                case -2
                    disp('MCL_DEV_ERROR');
                case -3
                    disp('MCL_DEV_NOT_ATTACHED');
                case -4
                    disp('MCL_USAGE_ERROR');
                case -5
                    disp('MCL_DEV_NOT_READY');
                case -6
                    disp('MCL_DEV_NOT_ATTACHED');
                case -7
                    disp('MCL_INVALID_AXIS');
                case -8
                    disp('MCL_INVALID_HANDLE');
                otherwise
                    disp('Unknown error code')
            end
        end
    end
    
end

