classdef MicroDriverController<handle
    %MICRODRIVERCONTROLLER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        handle;
        currentXPositionInSteps;
        currentYPositionInSteps;
        currentXPositionInMillimeter;
        currentYPositionInMillimeter;
        velocityInMillimeterPerS = 1;
        isMoving;
        maxVelocityInMillimeterPerS;
        minVelocityInMillimeterPerS;
        stepSizeInMillimeters;
    end
    
    methods
        function obj = MicroDriverController(libraryPath)
            obj.loadMicroDriveLibrary(libraryPath);
            obj.initialiseMicroDriveHandle();
        end
        
        function close(obj)
            calllib('MicroDrive', 'MCL_ReleaseHandle', obj.handle);
            unloadlibrary('MicroDrive');
            disp('Exiting');
        end
        
        function stepSize = get.stepSizeInMillimeters(obj)
            encoderResolution = 0;
            pointerEncoderResolution = libpointer('doublePtr', encoderResolution);
            
            stepSize = 0;
            pointerStepSize = libpointer('doublePtr', stepSize);
            
            maxVelocity = 0;
            pointerMaxVelocity = libpointer('doublePtr', maxVelocity);
            
            maxVelocityTwoAxis = 0;
            pointerMaxVelocityTwoAxis = libpointer('doublePtr', maxVelocityTwoAxis);
            
            maxVelocityThreeAxis = 0;
            pointerMaxVelocityThreeAxis = libpointer('doublePtr', maxVelocityThreeAxis);
            
            minVelocity = 0;
            pointerMinVelocity = libpointer('doublePtr', minVelocity);
            
            err = calllib('MicroDrive', 'MCL_MDInformation',...
                pointerEncoderResolution,...
                pointerStepSize,...
                pointerMaxVelocity,...
                pointerMaxVelocityTwoAxis,...
                pointerMaxVelocityThreeAxis,...
                pointerMinVelocity,...
                obj.handle);
            obj.printErrorMessage(err);
            stepSize = pointerStepSize.value;
        end
        
        function velocity = get.maxVelocityInMillimeterPerS(obj)
            encoderResolution = 0;
            pointerEncoderResolution = libpointer('doublePtr', encoderResolution);
            
            stepSize = 0;
            pointerStepSize = libpointer('doublePtr', stepSize);
            
            maxVelocity = 0;
            pointerMaxVelocity = libpointer('doublePtr', maxVelocity);
            
            maxVelocityTwoAxis = 0;
            pointerMaxVelocityTwoAxis = libpointer('doublePtr', maxVelocityTwoAxis);
            
            maxVelocityThreeAxis = 0;
            pointerMaxVelocityThreeAxis = libpointer('doublePtr', maxVelocityThreeAxis);
            
            minVelocity = 0;
            pointerMinVelocity = libpointer('doublePtr', minVelocity);
            
            err = calllib('MicroDrive', 'MCL_MDInformation',...
                pointerEncoderResolution,...
                pointerStepSize,...
                pointerMaxVelocity,...
                pointerMaxVelocityTwoAxis,...
                pointerMaxVelocityThreeAxis,...
                pointerMinVelocity,...
                obj.handle);
            obj.printErrorMessage(err);
            velocity = pointerMaxVelocity.value;
        end
        
        function velocity = get.minVelocityInMillimeterPerS(obj)
            encoderResolution = 0;
            pointerEncoderResolution = libpointer('doublePtr', encoderResolution);
            
            stepSize = 0;
            pointerStepSize = libpointer('doublePtr', stepSize);
            
            maxVelocity = 0;
            pointerMaxVelocity = libpointer('doublePtr', maxVelocity);
            
            maxVelocityTwoAxis = 0;
            pointerMaxVelocityTwoAxis = libpointer('doublePtr', maxVelocityTwoAxis);
            
            maxVelocityThreeAxis = 0;
            pointerMaxVelocityThreeAxis = libpointer('doublePtr', maxVelocityThreeAxis);
            
            minVelocity = 0;
            pointerMinVelocity = libpointer('doublePtr', minVelocity);
            
            err = calllib('MicroDrive', 'MCL_MDInformation',...
                pointerEncoderResolution,...
                pointerStepSize,...
                pointerMaxVelocity,...
                pointerMaxVelocityTwoAxis,...
                pointerMaxVelocityThreeAxis,...
                pointerMinVelocity,...
                obj.handle);
            obj.printErrorMessage(err);
            velocity = pointerMinVelocity.value;
        end
        
        function isMoving = get.isMoving(obj)
            initialMovingValue = 0;
            pointerIsMoving = libpointer('int32Ptr', initialMovingValue);
            err = calllib('MicroDrive', 'MCL_MicroDriveMoveStatus', pointerIsMoving, obj.handle);
            isMoving = pointerIsMoving.value == 1;
            obj.printErrorMessage(err);
        end
        
        function set.velocityInMillimeterPerS(obj,velocity)
            if velocity > obj.maxVelocityInMillimeterPerS
                obj.velocityInMillimeterPerS = obj.maxVelocityInMillimeterPerS;
                warningMessage = strcat('Input velocity larger than max allowed velocity. Velocity was set to: ',num2str(obj.maxVelocityInMillimeterPerS));
                warning(warningMessage);
            elseif velocity < obj.minVelocityInMillimeterPerS
                obj.velocityInMillimeterPerS = obj.minVelocityInMillimeterPerS;
                warningMessage = strcat('Input velocity smaller than min allowed velocity. Velocity was set to: ',num2str(obj.minVelocityInMillimeterPerS));
                warning(warningMessage); 
            else
                obj.velocityInMillimeterPerS = velocity;
            end
        end
        
        function position = get.currentXPositionInSteps(obj)
            xSteps = 0;
            pointerXSteps = libpointer('int32Ptr', xSteps);
            err = calllib('MicroDrive','MCL_MDCurrentPositionM',1,pointerXSteps, obj.handle);
            position = double(pointerXSteps.value);
            obj.printErrorMessage(err);
        end
        
        function position = get.currentYPositionInSteps(obj)
            ySteps = 0;
            pointerYSteps = libpointer('int32Ptr', ySteps);
            err = calllib('MicroDrive','MCL_MDCurrentPositionM',2,pointerYSteps, obj.handle);
            position = double(pointerYSteps.value);
            obj.printErrorMessage(err);
        end
        
        function position = get.currentXPositionInMillimeter(obj)
            xSteps = 0;
            pointerXSteps = libpointer('int32Ptr', xSteps);
            err = calllib('MicroDrive','MCL_MDCurrentPositionM',1,pointerXSteps, obj.handle);
            position = double(pointerXSteps.value) * obj.stepSizeInMillimeters;
            obj.printErrorMessage(err);
        end
        
        function position = get.currentYPositionInMillimeter(obj)
            ySteps = 0;
            pointerYSteps = libpointer('int32Ptr', ySteps);
            err = calllib('MicroDrive','MCL_MDCurrentPositionM',2,pointerYSteps, obj.handle);
            position = double(pointerYSteps.value) * obj.stepSizeInMillimeters;
            obj.printErrorMessage(err);
        end
        
        function moveMillimetersInXDirection(obj, distanceInMillimeter)
            disp('Moving in X direction');
            disp(distanceInMillimeter);
            err = calllib('MicroDrive','MCL_MDMove',1, obj.velocityInMillimeterPerS, distanceInMillimeter, obj.handle);
            obj.printErrorMessage(err);
        end
        
        function moveMillimetersInYDirection(obj, distanceInMillimeter)
            disp('Moving in Y direction');
            disp(distanceInMillimeter);
            err = calllib('MicroDrive','MCL_MDMove',2, obj.velocityInMillimeterPerS, distanceInMillimeter, obj.handle);
            obj.printErrorMessage(err);
        end
        
        function moveStepsInXDirection(obj,steps)
            disp('Moving in X direction');
            disp(steps);
            err = calllib('MicroDrive','MCL_MicroDriveMoveProfile_MicroSteps',1, obj.velocityInMillimeterPerS, steps, obj.handle);
            obj.printErrorMessage(err);
        end
        
        function moveStepsInYDirection(obj,steps)
            disp('Moving in Y direction');
            disp(steps);
            err = calllib('MicroDrive','MCL_MicroDriveMoveProfile_MicroSteps',2, obj.velocityInMillimeterPerS, steps, obj.handle);
            obj.printErrorMessage(err);
        end
    end
    
    methods(Access=private)
        
        function loadMicroDriveLibrary(obj,libraryPath)
            disp(pwd);
            loadlibrary('MicroDrive', libraryPath);
            if (~libisloaded('MicroDrive'))
                disp('Error: MicroDrive library did not load correctly');
                obj.close()
            end
        end
        
        function initialiseMicroDriveHandle(obj)
            obj.handle = calllib('MicroDrive', 'MCL_InitHandle');
            if (obj.handle == 0)
                disp('Error: Handle was not initialized correctly');
                obj.close()
            end
        end
        
        function printErrorMessage(obj,errorCode)
            switch errorCode
                case 0
                case -1
                    disp('MCL_GENERAL_ERROR');
                case -2
                    disp('MCL_DEV_ERROR');
                case -3
                    disp('MCL_DEV_NOT_ATTACHED');
                case -4
                    disp('MCL_USAGE_ERROR');
                case -5
                    disp('MCL_DEV_NOT_READY');
                case -6
                    disp('MCL_DEV_NOT_ATTACHED');
                case -7
                    disp('MCL_INVALID_AXIS');
                case -8
                    disp('MCL_INVALID_HANDLE');
                otherwise
                    disp('Unknown error code')
            end
        end
        
    end
end

