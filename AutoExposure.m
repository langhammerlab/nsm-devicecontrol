function AutoExposure(camera)
% Set camera exposure time automatically

camera.numberOfAccumulations = 1;
camera.exposureTime =  3e-5;
camera.triggerMode = 'Software';
%deltaT = 0.9239e-5;
deltaT = 0.5e-5;
maxIntensity = 0;

camera.startAcquisition();
while maxIntensity < (2^16*0.9)
    camera.exposureTime = camera.exposureTime + deltaT;
    [frame, time] = camera.getNextFrame();
    maxIntensity = max(frame(:));
end


camera.exposureTime = camera.exposureTime - deltaT;
[frame, time] = camera.getNextFrame();
camera.stopAcquisition();
disp('Exposure time set to:');
disp(camera.exposureTime);
disp('Max intensity:')
disp(max(frame(:)));

