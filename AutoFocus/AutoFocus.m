classdef AutoFocus
    %AUTOFOCUS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        microDriver
        nanoDriver
        camera
        pixelWidth % In micro meters
    end
    
    methods
        function obj = AutoFocus(microDriver,nanoDriver, camera, pixelWidth)
            obj.microDriver = microDriver;
            obj.nanoDriver = nanoDriver;
            obj.camera = camera;
            obj.pixelWidth = pixelWidth;
        end
        
        function [frames, times] = focusInX(obj)
            obj.camera.startAcquisition();
            [firstFrame, timeZero] = obj.camera.getNextFrame();
            centerOfMass = obj.findCenterOfMassInX(firstFrame(:,2:end-1));
            distanceToMoveInMillimeter = (centerOfMass+1 - obj.camera.imageWidth/2)*obj.pixelWidth * 0.001;
            disp(distanceToMoveInMillimeter);
            
            if abs(distanceToMoveInMillimeter) > 0.002
                obj.microDriver.velocityInMillimeterPerS = obj.microDriver.minVelocityInMillimeterPerS;
                timeForMoving = abs(distanceToMoveInMillimeter) / obj.microDriver.velocityInMillimeterPerS;
                nrOfFramesToAcquire = round(obj.camera.frameRate * timeForMoving + 1);
                frames = zeros(obj.camera.imageHeight, obj.camera.imageWidth, nrOfFramesToAcquire + 1);
                times = zeros(1, nrOfFramesToAcquire + 1);
                frames(:,:,1) = firstFrame;
                times(1) = timeZero;
                
                obj.microDriver.moveMillimetersInXDirection(distanceToMoveInMillimeter);
                for n=1:nrOfFramesToAcquire
                    [frames(:,:,n+1), times(n+1)] = obj.camera.getNextFrame();
                end
            elseif abs(distanceToMoveInMillimeter*1000) > 0.05
                distanceToMoveInMicrometer = distanceToMoveInMillimeter * 1000;
                disp('Distance in micrometers');
                disp(distanceToMoveInMicrometer);
                frames = zeros(obj.camera.imageHeight, obj.camera.imageWidth, 2);
                times = zeros(1, 2);
                frames(:,:,1) = firstFrame;
                times(1) = timeZero;
                obj.nanoDriver.moveDistanceInXDirection(-1.5*distanceToMoveInMicrometer);
                pause(0.2);
                [frames(:,:,2), times(2)] = obj.camera.getNextFrame();
            else
                frames = firstFrame;
                times = timeZero;
            end
            obj.camera.stopAcquisition();
        end
        
        function [frames, times] = focusInZ(obj,zStepInMicrometer)
            intitialTriggerMode = obj.camera.triggerMode;
            obj.camera.triggerMode = 'Software';
            obj.camera.startAcquisition();
            
            frames = zeros(obj.camera.imageHeight, obj.camera.imageWidth, 1);
            times = zeros(1,1);
            [frames(:,:,1), times(1)] = obj.camera.getNextFrame();
            intensity = max(sum(frames(:,5:end-5),1));
            previousIntensity = 0;
            while intensity > previousIntensity
                obj.nanoDriver.moveDistanceInZDirection(zStepInMicrometer);
                pause(0.2);
                [frames(:,:,end+1), times(end+1)] = obj.camera.getNextFrame();
                previousIntensity = intensity;
                intensity = max(sum(frames(:,5:end-5,end),1));
            end
            obj.nanoDriver.moveDistanceInZDirection(-zStepInMicrometer);
            previousIntensity = 0;
            while intensity > previousIntensity
                obj.nanoDriver.moveDistanceInZDirection(-zStepInMicrometer);
                pause(0.2);
                [frames(:,:,end+1), times(end+1)] = obj.camera.getNextFrame();
                previousIntensity = intensity;
                intensity =  max(sum(frames(:,5:end-5,end),1));
            end
            obj.nanoDriver.moveDistanceInZDirection(zStepInMicrometer);
            obj.camera.stopAcquisition();
            obj.camera.triggerMode = intitialTriggerMode;
        end
        
        
    end
    
    
    methods(Access=private)
        
        function centerOfMass = findCenterOfMassInX(obj, frame)
            intensity = sum(frame,1);
            normalised_intensity = (intensity-min(intensity))/(max(intensity)-min(intensity));
            normalised_intensity(normalised_intensity < 0.5) = 0;
            x = 1:1:length(normalised_intensity);
            centerOfMass = normalised_intensity*x'/sum(normalised_intensity);
            disp(centerOfMass);
        end
        
    end
end

