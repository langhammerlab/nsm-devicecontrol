%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NSM_DeviceControl
% Software used to acquire and save image data for NSM analysis
% Supported hardware
% Camera - Andor Zyla
% Micro controllers - Mad City Labs (X,Y)
% Nanopositioners - Mad City Labs (X,Y,Z)
%
% Required Software
% Andor MatlabSDK3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; close all;
% Inlcude required folders 
addpath('./ZylaCamera')
addpath('./AutoFocus')
addpath('./StageControllers')
addpath('./MadCityLabs')

% Metadata added to file
outputFolder='E:\results\Testfolder\';
info = 'Enter experiment information';
analyte = 'ANALYTE'; 
channelDimensions = [100, 100]; % Nanofluidic channel Dimensions (h,w) in nm
weight_stated =NaN; % Known weight of sample
title ='-';
pressure = 0;
nrOfMeasurements = 1;% Number of consequtive measurements before shutting down camera
nFrames = 10000; % Numnber of frames in each measurement
frameRate = 200; %Hz, Framerate in saved data

% Initiate camera
camera = ZylaCamera();

%Initialize micro/nano positioners
microDriverLibraryPath = 'C:/Users/lab/git/nsm-devicecontrol/MadCityLabs/MicroDrive.h';
nanoDriverLibraryPath = 'C:/Users/lab/git/nsm-devicecontrol/MadCityLabs/Madlib.h';
microDriver = MicroDriverController(microDriverLibraryPath);
nanoDriver = NanoDriverController(nanoDriverLibraryPath);
pixelWidth = 0.0295; % Width in micrometer for a single pixel

%% Camera settings

imageHeight = 550;
imageWidth = 30;
centerWidth = 1081;     % Center is at 1081
centerHeight = 1281;    % Center is at 1281 1251
camera.simplePreAmpGainControl = '16-bit (low noise & high well capacity)';
camera.electronicShutteringMode = 'Rolling';
camera.cycleMode = 'Continuous';
camera.triggerMode = 'Internal';
camera.pixelReadoutRate = '280 MHz';
camera.spuriousNoiseFilter = true;
camera.pixelEncoding = 'Mono32';
camera.fastAOIFrameRateEnable = true;
camera.metadataEnable = true;
camera.metadataTimestampEnable = true;
camera.setAreaOfInterest(imageHeight,imageWidth,centerHeight-imageHeight/2,centerWidth-imageWidth/2);
camera.exposureTime =  3e-5;
camera.numberOfAccumulations = 1;
camera.overlapReadout = true;
camera.sensorCooling = 1;

disp(['camera framerate: ' num2str(camera.frameRate)]);
disp(['camera maxtransfer rate: ' num2str(camera.maxInterfaceTransferRate)]);
disp(['camera exp. Time: ' num2str(camera.exposureTime)]);
pause(1);


% Automatic exposure setting
%AutoExposure(camera)

% Autofocus and reset the exposure after focusing
autoFocus = AutoFocus(microDriver, nanoDriver, camera, pixelWidth);
%AutoExposure(camera)


%% Measurement
% Set number of accumulations used to achieve requested framerate
camera.numberOfAccumulations = floor(camera.frameRate/frameRate);

for n=1:nrOfMeasurements
    disp('Camera temperature:');
    disp(camera.sensorTemperature);
    
    % Continous focusing
    autoFocusFrames = uint32(zeros(camera.imageHeight,camera.imageWidth));
    autoFocusFrames = AutoFocus_cont(camera,autoFocus,autoFocusFrames);
    
    % Set camera settings for saved images
    framerate1  = camera.frameRate;
    exposureTime = camera.exposureTime;
    camera.numberOfAccumulations = floor(camera.frameRate/frameRate);
    camera.horisontalBinning = imageWidth; % Bin perpendicular to channel
    
    
    disp('Camera temperature:');
    disp(camera.sensorTemperature);
    
    pause(1);
    frames = uint32(zeros(camera.imageHeight,nFrames));
    time = 0;
    

    
    
    % measure part
    liveplot=1;
   
    numacc=floor(camera.frameRate/frameRate);
    camera.numberOfAccumulations =  numacc;
    camera.stopAcquisition();
    camera.startAcquisition();
    
    % Initiate Live view figure
    if liveplot
        livefig=figure('Position',[100 200 1000 500]);
        display_accumulation = 10;
        display_width=100;
        x_image=0;
        liveimage = imagesc(1:display_width,1:imageHeight, ones(imageHeight,display_width));
        colormap bone
        caxis([1-3e-4 1+3e-4])
        n_lastplot=2;
    end
    
    % create progressbar
    fig = uifigure;
    d = uiprogressdlg(fig,'Title','NSM measurement',...
        'Message','Acuiring images','Cancelable','on');
    break_measurement=0;

    % Collect frames
    for k=1:nFrames
        %Get frame from cmera
        [frames(:,k),time] = camera.getNextFrame();
        d.Value = k/nFrames;
        %Update plot
        if mod(k,display_accumulation) == 0 && k>=2*display_accumulation && liveplot ==1
            disp(['frame nr: ' num2str(k)])
            x_image=x_image+1;           
            liveimage.CData(1:imageHeight,x_image) = double(sum(frames(:,k-display_accumulation+1:k),2))./double(sum(frames(:,k-2*display_accumulation+1:k-display_accumulation),2));          
            %drawnow;  % Uncomment if needed
            if x_image==display_width
                x_image=0;
                hold off
            end
        elseif mod(k,50) && liveplot == 0
            disp(['frame nr: ' num2str(k)])
        end
        if d.CancelRequested
            camera.stopAcquisition();
            break_measurement = 1;
            
            break
            
        end
    end
    camera.stopAcquisition();
    close(d);
    
    if break_measurement == 1
        camera.close();
        break;
        
    end
    % Show whole Kymograph
    figure('Position',[2 705 2732 273]);
    imagesc(double(frames(:,2:end))./double(frames(:,1:end-1)))
    caxis([1-5e-4 1+5e-4])
    colormap gray
    drawnow;
    
    % Create data object to save
    data = {};
    data.Im = double(frames);
    data.time = 0:1/camera.frameRate*camera.numberOfAccumulations:(nFrames-1)/camera.frameRate*camera.numberOfAccumulations;
    data.imageHeight = camera.imageHeight;
    data.imageWidth = camera.imageWidth;
    data.imageLeftPosition = camera.imageLeftPosition;
    data.imageTopPosition = camera.imageTopPosition;
    data.Xbinning = camera.horisontalBinning;
    data.Ybinning = camera.verticalBinning;
    data.exposureTime = camera.exposureTime;
    data.numberOfAccumulations = camera.numberOfAccumulations;
    data.frameRate = camera.frameRate;
    data.magnification = 220;
    data.pixelSize = 6.5;
    data.Yum = (1:camera.imageHeight)*data.pixelSize*data.Ybinning/data.magnification;
    %data.Xum = (1:camera.imageWidth)*data.pixelSize*data.Xbinning/data.magnification;
    data.info = info;
    data.analyte = analyte;
    data.channelDimensions = channelDimensions;
    data.date = datestr(now,'dd-mm-yy_HH-MM-SS');
    data.pressure = pressure;
    data.molecule.weight_stated = weight_stated;
    data.molecule.title = title;
    data.I=autoFocusFrames(:,:,end);
    
    
    save(strcat(outputFolder,'\',data.date,'_M.mat'),'data')
    disp('Measurements done:')
    disp(n);
    %pause(20);
    
    disp(['Number of accumulations: ' num2str(data.numberOfAccumulations)])
end
%% Close
answer = questdlg('Close camera and controllers?');
if strcmp('Yes',answer)
    camera.close();
    nanoDriver.close();
    microDriver.close();
end


